import React, {Component} from "react";
import {connect} from "react-redux";
import {getComments} from "../reducers/comments";

class _SillyComments extends Component {
  componentDidMount() {
    this.props.getComments();
  }
  render() {
    return (
      <div>
        {this.props.comments.length == 0 ? (
          <span>There is no comment yet, wait 2 seconds.</span>
        ) : null}
        {this.props.comments.map((comment, index) => (
          <div style={{border: "3px solid #000", margin: "40px"}}>
            <h3>Comment #{index + 1}</h3>
            <h4>{comment.name}</h4>
            <p>{comment.body}</p>
          </div>
        ))}
      </div>
    );
  }
}

export default connect(
  state => {
    return {comments: state.comments.myComments || []};
  },
  {getComments}
)(_SillyComments);
