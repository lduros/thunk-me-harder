import React, {Component} from "react";
import logo from "./logo.svg";
import "./App.css";
import SillyComments from "./components/SillyComments";

class App extends Component {
  render() {
    return (
      <div className="App">
        <SillyComments />
      </div>
    );
  }
}

export default App;
