import {FETCH_COMMENTS} from "../actions/comments";

export function getComments() {
  return dispatch => {
    // Here we'll request real (but fake) comments. Check out the URL to see what it looks like.
    fetch("https://jsonplaceholder.typicode.com/comments")
      .then(response => {
        return response.json();
      })
      .then(data => {
        return dispatch({
          type: FETCH_COMMENTS,
          payload: {data: data}
        });
      })
      .catch(e => {
        console.log("An error occurred while fetching comments", e);
      });
  };
}

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_COMMENTS:
      return {...state, myComments: action.payload.data};
    default:
      return state;
  }
}
