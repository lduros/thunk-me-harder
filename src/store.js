import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from "redux-thunk";
import comments from "./reducers/comments";

export const store = createStore(
  combineReducers({comments}),
  applyMiddleware(thunk)
);
